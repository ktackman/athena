#include "../FastTrackFinderLRTHypoTool.h"
#include "../FastTrackFinderLRTHypoAlg.h"
#include "../TrigIsoHPtTrackTriggerHypoAlgMT.h"
#include "../TrigIsoHPtTrackTriggerHypoTool.h"

DECLARE_COMPONENT (TrigIsoHPtTrackTriggerHypoAlgMT )
DECLARE_COMPONENT (TrigIsoHPtTrackTriggerHypoTool )
DECLARE_COMPONENT( FastTrackFinderLRTHypoTool )
DECLARE_COMPONENT( FastTrackFinderLRTHypoAlg )
